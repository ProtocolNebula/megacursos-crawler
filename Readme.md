# Mega Cursos Crawler

## NOTAS
Dado el "target" de usuarios de megacursos.com (usuarios de habla hispana), la documentación del código (de forma parcial o completa) está en **Español**


## Descripción
Este script permite la descarga de todos los cursos y sus archivos complementarios comprados en "MegaCursos.com".

Para utilizar este script necesitas:

* Cuenta en "[megacursos.com](https://megacursos.com/)"
* Comprar los cursos que quieras utilizar
* [NodeJS](https://nodejs.org) ```8.4 o superior```


**SIN UNA CUENTA EL SCRIPT NO FUNCIONA**

La intención del script es poder tener un **backup de los cursos** de forma local sin necesidad de la aplicación de megacursos.com

El uso ilegítimo del script y la distribución ilegal del contenido descargado es **responsabilidad de la persona que lo usa**.


## Puesta en marcha
**Puedes utilizar un manual si lo prefieres**: [Instalacion en debian](debian-install.md)

* Descargamos [NodeJS](https://nodejs.org) ``8.4 o superior``
* Clonamos/Descargamos el script del repositorio
* Copiamos el archivo ```src/config/account.original.js``` a ```src/config/account.js``` y lo **rellenamos** con la configuración correspondiente
* Desde el directorio del script ejecutamos:
```bash
npm install
npm start
```
* Esperamos a que se descarguen los cursos.

Si hay cualquier problema crea un nuevo Issue o haz fork y solvéntalo. ¡Recuerda hacer pull request!


## Iniciar el bot de telegram

1. Añade el bot al/los grupo/s de telegram que quieras
2. Escribe ```/start@NombreDelBot``` y listo
3. Para parar el bot ```/stop@NombreDelBot```
