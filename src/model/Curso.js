const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

class Curso {
    constructor() {
        this.nombre = null
        this.image = null
        this.url = null
        this.videos = [] // para cargarlos utilizar Crawler.getVideos(curso)
        
        this.descargado = false
        this.pesoTotal = 0
    }
    
    /**
     * Lee un objeto HTML generado desde "cheerio"
     * @param {type} root Objeto generado desde cheerio.load(HTML)
     * @param {type} curso Objeto HTML de cheerio con la informacion del curso actual
     *  Obtenido con: root('.dataBoxCourse', '.productContainer .row')[n]
     */
    parseHTML(root, curso) {
        this.image = root('.dataBoxRight2 img', curso).attr('src')
        this.nombre = entities.decode(root('.dataBoxLeft2 .pro_big2', curso).html()) // Carga el nombre (la 1a coincidencia)
        this.url = root('.dataBoxLeft2 a', curso).attr('href')
    }
}

module.exports = Curso