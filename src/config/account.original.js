const path = require('path');

module.exports = {
    URL_LOGIN: "https://megacursos.com/login",
    USERNAME: "",
    PASSWORD: "",
    
    // Ruta donde se almacenaran los archivos (absoluta o relativa a la ejecucion del script)
    // con / final
    DOWNLOAD_PATH: process.cwd() + path.sep + "downloads" + path.sep,
        
    // Si se especifica true, cargara TODOS los enlaces antes de comenzar a descargar
    // Recomendado activar en PRODUCCION
    // Ten en cuenta que en funcion del contenido puede tardar bastante, por eso se
    // recomienda deshabilitarlo en DESARROLLO
    LOAD_BEFORE_DOWNLOAD: true,
    
    // AVISO: tough-cookie-filestore esta bug (la version con la que se ha desarrollado)
    // y no genera correctamente el archivo de cookies, asi que al intentar recuperarlas 
    // no podrá leer el archivo y cargará las cookies por defecto
    SAVE_COOKIES: false, 
    
    // Especifica el tiempo de cache (en segundos)
    // Guarda los archivos HTML en un directorio "CACHE" (en la raiz del proyecto)
    // evitando asi tener que hacer los acceso todo el rato
    // Los archivos de cache solo se borraran si algun archivo lo reemplaza
    // 0 = deshabilita cache (no generara ningun html)
    SAVE_CACHE: 0,
    
    // Si se especifica se notificara a los grupos que añadan el bot de la API
    TELEGRAM_API: "",
 
    // URL con todos los archivos de todos los cursos
    // Publicar esta URL se puede considerar delito
    // Por lo tanto introducela únicamente si la tienes y bajo tu responsabilidad
    // Estos archivos se guardaran en la carpeta "EXTRA" de la raiz
    // No se bajaran los enlaces que esten cargados de los cursos normales, asi que no te preocupes por la redundancia :)
    URL_EXTRA_FILES: "",
}