const account = require('./config/account.js')
const crawler = require('./crawlers/megacursos.js')
const request = require('./class/request-service.js') // Start request with cookies support
const Helpers = require('./class/Helpers.js')
const Modulos = require('./class/ModulesManager.js')

const crawl = new crawler(request)

process.on('unhandledRejection', (err) => {
    console.error(err)
    //process.exit(1)
})

// Iniciamos los modulos
Modulos.iniciarModulos()

/**
 * Encapsulamos la parte principal del codigo para poder ejecutarlo asincronamente
 * @returns {undefined}
 */
async function execute() {
    try {
        // Iniciamos sesion por primera vez
        let response = await crawl.doLogin()
        console.log("Conectado como: " , account.USERNAME)
        
        // Cargamos los cursos y su contenido
        let cursos = await crawl.getCourses()
        
        Helpers.log("Encontrados " + cursos.length + " cursos")
        
        // Descargamos los archivos
        for (let c = 0; c < cursos.length; c++) {
            let curso = cursos[c]
            
            Helpers.log("Comenzando la descarga de archivos de <b>" + curso.nombre + "</b>", true)
            await crawl.descargarVideos(curso)
            //Helpers.log("Curso <b>" + curso.nombre + "</b> descargado\n")
        }
        
        Helpers.log("Todos los cursos descargados ", true)
   } catch (ex) {
        console.log("ERROR EN EL PROCESO PRINCIPAL: ")
        console.log(ex)
    }
}

// Ejecutamos el proceso asincrono
execute()
