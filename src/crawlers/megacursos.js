const account = require('../config/account.js')
const Curso = require('../model/Curso.js')
const Helpers = require('../class/Helpers.js');
const Video = require('../model/Video.js');
const path = require('path');
const fs = require('fs');
const S = require('string')

// TODO: Comprobar si cheerio se puede sustituir por xml2js (si sale rentable)
// Actualmente usamos cheerio porque para HTML es más fácil buscar clases, etc...
// Mientras que un XML deberia cumplir el mismo orden siempre
const cheerio = require('cheerio') // Parseador HTML con funcionalidad similar a JQUERY
const parseXML = require('xml2js').parseString // (Usado actualmente para extra files) Parseador XML (convierte a un objeto directamente)

class megacursos {
    constructor(request) {
        this.request = request
    }

    async doGet(url, cache = true) {
        var response = false
        
        if (cache) {
            response = Helpers.getFromCache(url)
        }
        
        if (!response) {
            response = await this.request({
                url: url,
                method:"GET",
            })

            if (cache) {
                if (response != "") {
                    // Guardamos en cache
                    await Helpers.saveCache(url, response)
                } else {
                    // Intentamos recuperar los datos de la cache
                    response = Helpers.getFromCache(url, true)
                }
            }
        }
        
        return response
    }

    /**
     * Comprueba si hay una sesion activa
     * @param {type} html Si se especifica el HTMl se comprobara sobre esta respuesta
     * @returns {Boolean}
     */
    async isLogued(html = '') {
        if (!html || html == '') {
            html = await this.doGet("https://megacursos.com/my-account", false)
        }
        
        return (html.indexOf("https://megacursos.com/logout") > -1) 
    }

    /**
     * Realiza una conexion en la pagina web
     * @returns {Boolean}
     */
    async doLogin() {
        
        if (await this.isLogued()) {
            return true
        }
        
        //return new Promise((resolve, reject) => {
        let response
        try {
            response = await this.request({
                url:account.URL_LOGIN,
                method:"POST",
                form:{
                    email:account.USERNAME,
                    password:account.PASSWORD,
                    submit: ""
                }
            })  
            if (response == "") return true;
            
        } catch (exception) {
            console.log("ERROR: " ,exception)
        }
        
        throw new Error("Hubo un problema al iniciar sesion")
        
        /*await this.request({
            url:account.URL_LOGIN,
            method:"POST",
            form:{
                email:account.USERNAME,
                password:account.PASSWORD,
                submit: ""
            }
        },
        function(error,response,body){
            if (error === null) {
                resolve()
            } else {
                reject("Bad user or passowrd or invalid URL")
            }
        });*/
    }
    
    /**
     * Obtiene el listado de cursos de la cuenta del usuario
     * @returns {Array|nm$_Base.Base.getCourses.cursos|Base.getCourses.cursos}
     */
    async getCourses() {
        let cursos = []
        /*await this.getVideosExtra(cursos)
        console.log(cursos)
        process.exit(1)*/
        // Cargamos el contenido de la pagina
        let html = await this.doGet("https://megacursos.com/my-account")
        
        // Creamos el contenedor html
        let root = cheerio.load(html)
        
        // Buscamos los contenedores de cursos
        let CursosRaw = root('.dataBoxCourse', '.productContainer .row')

        for (let n=0; n < CursosRaw.length - 1; n++) {
            // Parseamos el curso usando el model
            let curso = new Curso()
            curso.parseHTML(root, CursosRaw[n])
            
            // Cargamos todos los enlaces
            if (account.LOAD_BEFORE_DOWNLOAD) {
                await this.getVideos(curso)
            }
            cursos.push(curso)
        }

        //await this.getVideosExtra(cursos)
        
        return cursos
    }
    
    /**
     * Descarga los videos del indice de archivos
     * Para mas informacion revisa "account.original.js" -> UR_EXTRA_FILES
     * @param {type} cursos
     * @returns {undefined}
     */
    async getVideosExtra(cursos) {
        try {
            if (account.URL_EXTRA_FILES) {
                // Cargamos el XML
                let xml = await this.doGet(account.URL_EXTRA_FILES)

                // 
                parseXML(xml, function (err, result) {
                    console.dir(result.ListBucketResult.Contents[0]);
                })
            }
            
        } catch (ex) {
            console.log("Error al leer el xml de extras:")
            console.log(ex)
        }
        
    }
    
    
    /**
     * Carga los videos (y archivos) de un curso especifico
     * @param Curso curso Objeto del curso
     * @returns Curso (añadiendo los videos de este)
     */
    async getVideos(curso) {
        await Helpers.wait(2000) // Esperamos un momento para evitar spam
        
        let html = ''
        
        // Si tenemos el html vacio puede ser que se haya desconectado la sesion
        while (html == '') {
            html = await this.doGet(curso.url)
            let isLogued = await this.isLogued(html)
        
            // Si se ha desconectado intentamos reconectarnos...
            if (!isLogued) {
                html = '' // Vaciamos el html para repetir el bucle
                Helpers.log("Error de conexion, reconectando a la cuenta ...")
                this.doLogin()
                await Helpers.wait(3000) // Esperamos 3 segundos para no hacer DDOS
            }
        }
        
        // Creamos el contenedor html
        let root = cheerio.load(html)
        
        // Buscamos los contenedores de videos
        let ListadoRaw = root('.panel', '.playerpagesyllabus #collapseOne')
        let video
        
        for (let n=0; n<ListadoRaw.length; n++) {
            let seccion = ListadoRaw[n]
            let nombre_seccion = Helpers.decodeHTML(root('.panel-heading .panel-title a', seccion).html().trim())
            let VideosRaw = root('.panel-body .boxRow', seccion)
            
            Helpers.log("Leyendo seccion: <b>" + nombre_seccion + "</b>")
            for (let v=0; v<VideosRaw.length; v++) {
                try {
                    video = new Video()
                    video.nombre_seccion = nombre_seccion
                    video.parseHTML(root, VideosRaw[v]);
                    curso.videos.push(video)
                } catch (ex) {
                    Helpers.log(ex)
                    console.log(ex)
                    console.log(video)
                    process.exit(1) // Si queremos parar para evitar errores antes de comenzar la descarga...
                }
            }
        }
        
        return curso
    }
       
    /**
     * Descarga los videos de un curso
     * @param {Curso} curso
     * @returns {undefined}
     */
    async descargarVideos(curso) {
        // Cargamos todos los enlaces si no se han cargado antes
        if (!account.LOAD_BEFORE_DOWNLOAD) {
            await this.getVideos(curso)
        }
        
        let descarga = false // Si se descarga algo pasa a true (para notificar o no por telegram)
        
        // Si se han encontrado videos comenzamos el proceos de descarga
        if (curso.videos.length > 0) {
            
            for (let n=0; n<curso.videos.length; n++) {
                let video = curso.videos[n]
                
                Helpers.log("Descargando contenido de <b>" + video.nombre + "</b>")
                let paths = this.getFilesPaths(curso, video)
                
                // Comprobamos el directorio
                if (!fs.existsSync(paths[0])) {
                    Helpers.makeDir(paths[0])
                    Helpers.log("Creado el directorio " + paths[0])
                }
                
                // Comenzamos la descarga desde el indice 1
                for (let f=1; f<paths.length; f++) {
                    let file = paths[f][0]
                    let tmpFile = file + ".tmp"
                    let remoteFile = paths[f][1]
                    
                    if (fs.existsSync(file)) {
                        if (f > 1 || fs.statSync(file).size > 10000) {
                            // Si EXISTE y NO es el primer archivo (video) o el archivo es mayor de 9 MB aprox...
                            Helpers.log(paths[f][1] + " ya esta descargado")  
                            continue
                        }
                    } 
                    
                    let downloaded = false
                    let notificacionError = false // Si es true evitaremos volver a notificar de un error

                    // reintentos infinitos hasta que se descargue
                    while (!downloaded) {
                        Helpers.log("Descargando " + remoteFile)
                        try {
                            await Helpers.downloadFile(
                                remoteFile,
                                tmpFile
                            )
                            fs.renameSync(tmpFile, file)
                            downloaded = true
                            descarga = true
                            Helpers.log("Archivo descargado")
                        } catch (ex) {
                            Helpers.log("Error al descargar el archivo: " + ex, !notificacionError)
                            notificacionError = true // Evitamos el volver a avisarlo
                            console.log(ex)
                            await Helpers.wait(28000) // Esperamos un tiempo para evitar que nos bloqueen
                        }
                        await Helpers.wait(2000) // Esperamos un tiempo para evitar que nos bloqueen
                    }
                    
                    // Calculamos cuanto ocupa el archivo
                   video.peso += Math.floor(fs.statSync(file).size / 1024 / 1024) // Guardamos el tamaño en MB para evitar overflows
                }
                
                // marcamos el video como descargado
                video.descargado = true
                
                // Sumamos el peso total descargado al peso del curso
                curso.pesoTotal += video.peso
                
                Helpers.log("=============")
                
            }
            
            if (descarga) {
                Helpers.log("Curso " + curso.nombre + " descargado (~ " + curso.pesoTotal / 1024 + " GB)", true)
            } else {
                Helpers.log("Total descargado: " + curso.pesoTotal + " MB")
            }
            curso.descargado = true
            Helpers.log("################")
            
        } else {
            Helpers.log("No se han encontrado videos para el curso <b>" + curso.nombre + "</b>", true)
        }
    }
    
    
    /**
     * Devuelve las rutas de los archivos a descargar y donde guardarlos
     * NOTA: El primer indice ES UN STRING, NO UN ARRAY, especifica el directorio donde se guardaran los archivos
     * @param {Curso} curso 
     * @param {Video} video
     * @returns {array} 
     *      0 => ruta al directorio
     *      1 (o mayor) =>
     *      [ RUTA ARCHIVO LOCAL COMPLETA, URL A DESCARGAR ]
     */
    getFilesPaths(curso, video) {
        let paths = []
        
        // Ruta al directorio
        video.nombre_seccion = video.nombre_seccion.replace(curso.nombre, '')
        paths.push (
            account.DOWNLOAD_PATH + Helpers.parseForPath(
                curso.nombre + path.sep + 
                video.nombre_seccion + path.sep + 
                video.numero + ' - ' + video.nombre + path.sep
            )
        ) 
        
        // Ruta al video
        let videoURL = video.video
        if (videoURL != "") {
            let videoString = S(videoURL)
            // Contamos si tiene 2 enlaces (o mas), asumiremos que el enlace correcto es el ULTIMO
            if (videoString.count('http://') > 2) {
                videoURL = this.getLastUrlFromVideo(videoURL, 'http://')
            } else if (videoString.count('http://') > 2) {
                videoURL = this.getLastUrlFromVideo(videoURL, 'https://')
            }
            
            paths.push ([
                paths[0] + Helpers.parseForPath("video" + Helpers.getExt(videoURL)), // .replace(/((\?|#).*)?$/,'')
                video.video
            ])
        }
        
        if (video.archivos) {
            if (video.archivos.indexOf('ArchivosServidorExterno.txt') == -1) {
                // Ruta al archivo
                let archivo = video.archivos
                archivo = archivo.substring(archivo.lastIndexOf('/')+1).replace(/((\?|#).*)?$/,'');
                paths.push ([
                    paths[0] + Helpers.parseForPath(archivo),
                    video.archivos
                ])
            } else {
                // Ruta para el archivo generico
                paths.push([
                    paths[0] + '../ArchivosServidorExterno.txt',
                    video.archivos
                ])
            }
        }
        
        return paths
    }
    
    
    /**
     * Obtiene el ultimo enlace dentro de "url" si un video tiene mas de 1 url
     * @param {string} urls Lista de urls
     * @param {string} protocolo ( http:// | https:// | ... )
     * @returns {string} Ultima url encontrada
     */
    getLastUrlFromVideo(urls, protocolo) {
        let urlsSplit = urls.split(protocolo)
        
        return protocolo + urlsSplit[urlsSplit.length - 1]
    }

}

module.exports = megacursos
