var log = ""
const account = require('../config/account.js')
const http = require('http');
const https = require('https');
const fs = require('fs');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();
const RemoveAccents = require('remove-accents');

const path = require('path');
var shell = require('shelljs');
const Modulos = require('./ModulesManager.js')

class Helpers {

    static currentDate() {
        return new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
    }

    static async log(text, enviarNotificacion = false) {
        let currentLine = "[" + Helpers.currentDate() + "] " + RemoveAccents(text)
        console.log(currentLine)
        
        if (enviarNotificacion) {
            Modulos.notificarMensaje(text)
        }

        log += currentLine + "\n"
    }

    static getLog() {
        return log
    }

    static wait(timeout) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve()
            }, timeout)
        })
    }
    
    /**
     * Crea un directorio en una ruta especificada con los directorios padre
     * @param {type} path
     * @returns {undefined}
     */
    static makeDir(path) {
        if (!fs.existsSync(path)) {
            shell.mkdir('-p', path)
        }
    }
    
    /**
     * Descarga un archivo de una url a un directorio especifico
     * @param {type} url URL a descargar
     * @param {type} dest Ruta hacia el archivo (nombre del archivo incluido)
     * @returns {undefined}
     * @url https://stackoverflow.com/questions/11944932/how-to-download-a-file-with-node-js-without-using-third-party-libraries
     */
    static downloadFile(url, dest) {
        return new Promise((resolve, reject) => {
            var callBack = function() {
                resolve()
            }
            
            let httpProtocol = (url.indexOf('https://') == 0) ? https : http
            
            let file = fs.createWriteStream(dest);
            let request = httpProtocol.get(url, function (response) {
                response.pipe(file);
                file.on('finish', function () {
                    file.close(callBack);  // close() is async, call cb after close completes.
                });
            }).on('error', function (err) { // Handle errors
                fs.unlink(dest); // Delete the file async. (But we don't check the result)
                
                Helpers.log("Error al descargar " + url)
                reject()

            });
        })
    }
    
    static decodeHTML(html) {
        return entities.decode(html)
    }
    
    static RemoveAccents(text) {
        return RemoveAccents(text)
    }
    
    /**
     * Devuelve la extension de un archivo
     * @param {type} path
     * @returns {string} Extension de un archivo (".ext")
     */
    static getExt(pathString) {
        return path.extname(pathString)
    }
    
    /**
     * Devuelve un string con caracteres especiales eliminados (excepto / and \)
     * para poder utilizarlos de ruta
     * Ademas elimina los acentos de los caracteres
     * @param {type} text
     * @returns {undefined}
     */
    static parseForPath(text, mantainSpaces = true) {
        text = this.RemoveAccents(text)
        if (mantainSpaces) {
            return text.replace(/[^a-zA-Z0-9\. /\\-]/g, "").replace('  ', ' ')
        } else {
            return text.replace(/[^a-zA-Z0-9\./\\-]/g, "")
        }
    }
    
    
    /**
     * Convierte una url a un nombre de archivo
     * @param {type} url
     * @returns {unresolved}
     */
    static parseURLForFile(url) {
        return url.replace(/[^a-zA-Z0-9.-]/g, "_")
    }
    
    
    /**
     * Obtiene la ruta al directorio de cache
     * @returns {String}
     */
    static getCacheFolder() {
        return process.cwd() + path.sep + "cache" + path.sep
    }
    
    /**
     * Carga un archivo desde la cache, si no existe o ha caducado de vuelve false
     * NOTA: NO BORRA ARCHIVOS si han caducado
     * @param {string} url
     * @param {bool} ignoreTime Si esta activa ignorara el tiempo de vida del archivo
     *  Util si se ha caido/falla la conexion (volvemos a llamar a esta funcion si es necesario)
     * @returns {string}
     */
    static getFromCache(url, ignoreTime = false) {
        if (account.CACHE > 0) {
            let file = this.getCacheFolder() + this.parseURLForFile(url)
            
            if (fs.existsSync(file)) {    
                if (!ignoreTime) {
                    // Comprobamos si el archivo ha caducado
                    let fileTime = Math.floor(fs.statSync(file).mtimeMs / 1000)
                    let currentTime = Math.floor(Date.now() / 1000);
                    
                    if (currentTime > fileTime + account.CACHE) {
                        // Cache caducada
                        return false;
                    }
                }
                                
                //let response = fs.readFileSync(file, 'utf8')
                return fs.readFileSync(file, 'utf8')
            }
        }
        
        return false
    }
    
    /**
     * Guarda un archivo en la cache si esta habilitada
     * @param {type} url URL de la descarga 
     * @param {type} content Contenido del archivo
     */
    static async saveCache(url, content) {
        if (account.CACHE > 0) {
            let file = this.parseURLForFile(url)
            let cacheFolder = this.getCacheFolder()
            
            this.makeDir(cacheFolder)
            
            fs.writeFile(cacheFolder + file, content, 'utf8', function(err) {
                if (err) {
                    console.log('Error guardando el archivo de cache: ');
                    console.log(err)
                }
            })
        }
        
    }
}

module.exports = Helpers