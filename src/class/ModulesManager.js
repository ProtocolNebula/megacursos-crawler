const account = require('../config/account.js')
const Telegraf = require('telegraf')
const commandParts = require('telegraf-command-parts')
const Helpers = require('../class/Helpers.js')

/*
 * Este script tiene la intencion de en un futuro ser un gestor de modulos
 * Su funcion sera permitir a los modulos registrarse y notifdicarlos (listener)
 * Actualmente se ha improvisado unicamente para telegram
 */

/**
 * Mantiene abierta la conexion de telegram si hubiera
 * @type type
 */
var telegram = []

class ModulesManager {

    static iniciarModulos() {
        this.iniciarTelegram()
    }

    static notificarMensaje(mensaje) {
        this.notificarTelegram(mensaje)
    }

    /**
     * Inicia el "modulo" de telegram
     * @returns {undefined}
     */
    static async iniciarTelegram() {
        if (account.TELEGRAM_API != "") {
            telegram[0] = new Telegraf(account.TELEGRAM_API)
            telegram[0].telegram.getMe().then((botInfo) => {
                telegram[0].options.username = botInfo.username

                console.log("Bot username: @" + botInfo.username)
            })
            
            telegram[0].use(commandParts());
            telegram[0].use((ctx, next) => {
                try {
                    if (ctx) {
                        this.isAdminTelegram(ctx).then((res) => {
                            if ('command' in ctx.contextState) {
                                switch (ctx.contextState.command.command) {
                                    case "stop":
                                        this.RemoveTelegram(ctx)
                                        break;
                                    case "start":
                                        this.JoinTelegram(ctx)
                                        break;
                                    default:
                                        return next()
                                }
                            }
                        }).catch((error) => {
                            //ctx.reply("Only admins can use the bot")
                            console.log("Error reading command: " , error)
                        })
                    } else {
                        return next()
                    }
                } catch (ex) {
                    console.log('Parsing commands: ' + ex)
                    return
                }
            });
            
            telegram[0].startPolling()
            
            // Conexiones activas
            telegram[1] = []
            
        }
    }
    
    static RemoveTelegram(ctx) {
        const chat = ctx.update.message.chat
        delete telegram[1][chat.id]
        console.log("Grupo eliminado: " , currentId)
    }
    
    static JoinTelegram(ctx) {
        ctx.getChat().then((info) => {
            let currentId = info.id
            telegram[1][currentId] = ctx
            console.log("Nuevo Grupo: " , currentId)
        })
    }
    
    static isAdminTelegram(ctx) {
        return new Promise(function(resolve, reject) {
            ctx.getChat().then(function(chat) {

                // Check if all users are admin
                if (chat.all_members_are_administrators) {
                    resolve()
                    return
                }

                // Check if user is admin
                ctx.getChatAdministrators().then(function(admins) {
                    const currentUser = ctx.update.message.from.id

                    for (let key in admins) {
                        if (admins[key].user.id == currentUser) {
                            resolve()
                            return
                        }
                    }

                    reject()
                })

            })
        })

    }
    
    static async notificarTelegram(mensaje) {
        if (account.TELEGRAM_API != "") {
            for (let socket in telegram[1]) {
                
                try {
                    telegram[1][socket].reply(mensaje)
                } catch (ex) {
                    console.log(ex)
                    delete telegram[1][socket]
                }
                
            }
        }
    }
}


module.exports = ModulesManager