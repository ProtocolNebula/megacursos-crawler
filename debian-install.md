# Instalacion de dependencias
**NOTA:** Recomiendo usar ```tmux``` o ```screen``` para dejarlo en 2o plano, en el tutorial instalaré ```screen```

```bash
apt-get update
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs  git screen
```

# Montaje del crawler
**NOTA:** Cambiar /ruta/del/crawler por el directorio donde quieras que se ejecute y descargue

Ten en cuenta que dentro de este directorio se creará otro al clonar el proyecto.

```bash
cd /ruta/del/crawler
git clone https://gitlab.com/ProtocolNebula/megacursos-crawler.git
cd megacursos-crawler
cp src/config/account.original.js src/config/account.js
vim src/config/account.js
```

**Rellenamos los datos deseados. El bot de Telegram es opcional.**

# Iniciar el crawler
**Desde el directorio de ```megacursos-crawler```**
```bash
screen
npm start
```

## Salir del screen dejandola operativa
```Ctrl``` + ```a``` y después ```d```

## Reabrir screen
```bash
screen -r
```
